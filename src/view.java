import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;




	public class view extends JFrame {
		String list[] = {"Type 1", "Type 2", "Type 3", "Type 4"};
		private JButton button ;
		private JTextArea input ;
		private JTextArea output ;
		private JComboBox box ;
		
		 public void createComboBox()
		   {
			 String list[] = {"Type 1", "Type 2", "Type 3", "Type 4"};
			 box = new JComboBox(list);
		   }
		
		 public void createTextArea()
		   {
			  input = new JTextArea();
				 output = new JTextArea();
		   }
		
		public void createPanel()
		   {
			
			
			button.setBounds(10, 70, 260, 20);
			input.setBounds(10, 40,260, 20);
			output.setBounds(10, 100, 260, 340);
			box.setBounds(10, 10, 260, 20);
					
			add(box);
			add(input);
			add(output);
			add(button);
		   } 
		
		
		public view() {
			
			setSize(300,500);		
			setLayout(null);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		}
		
		public int getNum() {
			int num = Integer.parseInt(input
					.getText());
			return num;
		}
		
		public String getBox() {
			return (String) box.getSelectedItem() ;
		}
		
		 public void setText(String star) {
			 output.setText(star);
		   }
		 
		 public void createButton(ActionListener list)
		   {
			  button = new JButton("Run");
		      button.addActionListener(list);

		   }

	}
